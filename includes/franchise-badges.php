// Name:            Franchise Badges
// Description:     Defines styles for the Franchise Badges
//
// ========================================================================

// Import any common files
// @import "homepage/[file-name].less";

// Change these to your franchises,
// in the order that they appear in your sprite
@franchises : 
    "alfa-romeo", 
    "abarth", 
    "fiat", 
    "nissan", 
    "vauxhall", 
    "mg", 
    "infiniti",
    "subaru", 
    "jeep"
;

@franchise-badge--width-xs                      : 100% / 3;
@franchise-badge--width-sm                      : @franchise-badge--width-xs;
@franchise-badge--width-md                      : @franchise-badge--width-sm;
@franchise-badge--width-lg                      : @franchise-badge--width-md;

/* ==========================================================================
   Module: Franchise Badges
   ========================================================================== */

// Find and replace CONTAINER-NAME with your container
.CONTAINER-NAME {

    .jump {
        float: left;
        padding: 0;
        width: @franchise-badge--width-xs;
    }

    .-sprite-loop(@i: length(@franchises)) when (@i > 0) {
        @name : extract(@franchises, @i);
        @val : ~"@{name}";
        .@{val} .content {
            #custom-mixins > .create-sprite(100%; @i; length(@franchises));
        }
        .-sprite-loop((@i - 1));
    } .-sprite-loop;
}

.hook-responsive-sm() {

    .CONTAINER-NAME {

        // Remove float and inline-block .jump to align badges centrally
        // .row-fluid {
        //  text-align: center;
        // }
        
        .jump {
            width: @franchise-badge--width-sm;
        }
    }
}

.hook-responsive-md() {

    .CONTAINER-NAME {
        
        .jump {
            width: @franchise-badge--width-md;
            overflow: hidden;

            .content {
                transform: none;
                transition: all .25s;
            }

            &:hover {

                .content {
                    transform: scale(1.1);
                }
            }
        }
    }
}

.hook-responsive-lg() {

    .CONTAINER-NAME {
        
        .jump {
            width: @franchise-badge--width-lg;
        }
    }
}