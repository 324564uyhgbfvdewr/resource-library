.create-sprite (@sprite-padding-bottom: 0; @sprite-number; @sprite-length: 0; @sprite-folder: ''; @sprite-name: 'franchise-badges.png'; @sprite-offset-top: 50%) {
	@sprite-path : "../images/";
	@imageNumber : @sprite-number - 1;
	@numberImages : @sprite-length - 1;
	display: block;
	background-position: (100% * @imageNumber / @numberImages) 0;
	background-image: url("@{sprite-path}@{sprite-folder}@{sprite-name}");
	background-size: 100% * @sprite-length;
	height: 0;
	padding-bottom: @sprite-padding-bottom;
	transition: all 0.4s;
}