@socialicons--background_hover : 
	email @socialicons__email--background, 
	facebook @socialicons__facebook--background, 
	twitter @socialicons__twitter--background,
	google @socialicons__google--background, 
	googleplus @socialicons__google--background, 
	youtube @socialicons__youtube--background, 
	pinterest @socialicons__pinterest--background, 
	linkedin @socialicons__linkedin--background, 
	blogger @socialicons__blogger--background, 
	instagram @socialicons__instagram--background
;

.social-icons {

	li a {
		box-shadow: none;
		background: transparent;
	}

	.socialHoverBackgrounds(@iterator:1) when(@iterator <= length(@socialicons--background_hover)) {
		@name: extract(extract(@socialicons--background_hover, @iterator), 1);

		.@{name}:hover a {
			background-color: extract(extract(@socialicons--background_hover, @iterator), 2);
		}
		.socialHoverBackgrounds((@iterator + 1));
	}
	.socialHoverBackgrounds();
}