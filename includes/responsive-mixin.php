.responsive-shape(@color, @height, @position: top, @distance: 0, @color2: @color, @height2: @height, @position2: bottom, @distance2: 0) {
	
	.custom-html.shapes svg {
		position: absolute;
		@{position}: @distance;
		right: 0;
		left: 0;
		width: 100%;
		fill: @color;
		height: @height;

		+ svg {
			top: auto;
			@{position2}: @distance2;
			fill: @color2;
			height: @height2;
		}
	}
}	