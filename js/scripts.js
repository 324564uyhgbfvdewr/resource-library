// Prevent default behaviour of all hrefs
$('a').click(function(e) {
	e.preventDefault();
});

// js-absolute - group title, content and button together.
$('.js-absolute').each(function() {
	$container = $('<div class="text" />').appendTo($(this).find('.inner>a'));
	$(this).find('.title, .sub-title, .strapline, .content, .button').appendTo($container);
});

// Take span content and place it on the .btn title attr
$('.jump').each(function() {
	$buttonAttr = $(this).find('.btn span').html();
	$(this).find('.btn').attr('title', $buttonAttr);
});

// Clipbaord copy function
function ClipCopy(button) {
	new Clipboard(button, {
		target: function(trigger) {
			$('.overlay').fadeIn(250).delay(550).fadeOut();
			return trigger.nextElementSibling;
		}
	});
}

// Call clipboard copy function
ClipCopy('#buttons .btn');
ClipCopy('.html-copy');
ClipCopy('#jumps .jump a');
ClipCopy('#sidebars .row-fluid')
ClipCopy('#social .social-icons ul');
ClipCopy('#shapes .span4 svg');
ClipCopy('#franchises .jump a');
ClipCopy('#pagination .cycle-pager-container ul');

// Navigation smooth-scroll
$('.nav a').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 600);
			return false;
		}
	}
});

// Toggle Pagination
$('.pag-toggle').click(function() {
	$('.cycle-pager-container li:first-child').toggleClass('cycle-pager-active');
});

// Show sidebar text if hidden behind icon
$(window).load(function(){
    if($('[href*="/templateEditor.css"]').length == 0) {

	    $('.row-cjpal').addClass('is-visible');
	    setTimeout(function(){
            $('.row-cjpal').removeClass('is-visible');
        }, 1000);     		
    }
});
