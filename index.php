<!DOCTYPE html>
<html>
	<head>
		<title>CSS Resource Library</title>
		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="styles.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="https://cdn.rawgit.com/zenorocha/clipboard.js/v1.5.5/dist/clipboard.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>

	<body>
		<div class="nav">
			<ul>
				<li><a href="#buttons">Buttons</a></li>
				<li><a href="#jumps">Jumps</a></li>
				<li><a href="#sidebars">Sidebars</a></li>
				<li><a href="#social">Social Icons</a></li>
				<li><a href="#pagination">Pagination</a></li>
				<li><a href="#shapes">Responsive Shapes</a></li>
				<li><a href="#franchises">Franchise Badges</a></li>
			</ul>
		</div>

		<div class="overlay"></div>

		<div class="container" id="buttons">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>
			<div class="row">
				<h1>Standard Buttons</h1>
				<div class="sub-text">Remember to place your LESS in <span>mixins.less</span> if you want it to apply to ALL buttons.</div>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, name, styles, class, icon FROM buttons";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$class = $row['class'];
							$name = $row['name'];
							$icon = $row['icon'];
							$styles = $row['styles'];
							echo "<div class='span'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo "	<div class='button " . $class . "'>";
							echo "		<a href='#' title='" . $name . "' class='btn'>";
							echo "			<i class='" . $icon . "'></i>";
							echo "			<span>" . $name . "</span>";
							echo "		</a>";
							echo "		<pre>" . $styles . "</pre>";
							echo "	</div>";
							echo "</div>";
						}
					}

					$conn->close();
				?>
			</div>

		</div>

		<div class="container" id="jumps">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>			
			<div class="row">
				<h1>Jumps</h1>
				<div class="sub-text">Some Jumps will require the <span>.js-absolute</span> class. The JavaScript will be supplied with them. Remember to place your hover LESS in the <span>.hook-responsive-md()</span> hook, and your styles in <span>.hook-responsive-sm()</span>. Hover effects aren't needed at tablet and below.</div>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, button, class, content, strapline, styles, subtitle, title FROM jumps";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$button = $row['button'];
							$class = $row['class'];
							$content = $row['content'];
							$strapline = $row['strapline'];
							$styles = $row['styles'];
							$subtitle = $row['subtitle'];
							$title = $row['title'];


							echo "<div class='span4 jump " . $class .  " module'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo "		<div class='inner'>";
							echo "			<a href='#' title='" . $button . "'>";
							echo "				<span class='thumb'>";
							echo "					<span class='frame'></span>";
							echo "					<div class='responsive-image'>";
							echo "						<figure class='responsive-image'>";
							echo "							<picture>";
							echo "								<img src='img/placeholder.jpg' class='responsive-image'>";
							echo "							</picture>";
							echo "						</figure>";
							echo "					</div>";                                                  
							echo "				</span>";
							echo " 				<span class='title'>";
							echo "					<span>" . $title . "</span>";
							echo "				</span>";
							echo " 				<span class='strapline'>";
							echo "					<span>" . $strapline . "</span>";
							echo "				</span>";
							echo " 				<span class='sub-title'>";
							echo "					<span>" . $subtitle . "</span>";
							echo "				</span>";
							echo " 				<span class='content'>";
							echo "					<span>" . $content . "</span>";
							echo "				</span>";
							echo "              <span class='button'>";
							echo "                  <span class='btn'>";
							echo "                     <span>" . $button . "</span>";
							echo "                 </span>";
							echo "              </span>";
							echo "			</a>";
							echo "			<pre>" . $styles . "</pre>";
							echo "		</div>";
							echo "    </div>";
						}
					}

					$conn->close();
				?>
			</div>

		</div>
		
		<div class="container" id="sidebars">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>
			<div class="row">
				<h1>Sidebars</h1>
				<div class="sub-text">These sidebars are accompanied by plenty of variables, so modifying them should be simple. You'll need the HTML with the correct container and row class names for these styles to work. <a href="#" class="html-copy">Click here</a><pre><?php include 'includes/sidebar-template.php';?></pre> to copy the HTML.</div>
				<pre></pre>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, class, styles FROM sidebars";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$class = $row['class'];
							$styles = $row['styles'];

							echo "<div class='span4'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo " 	<div class='container-wrap container-p0j3y " . $class . "'>";
							echo " 	    <div class='container'>";
							echo " 	        <div class='row-fluid row-cjpal'>";
							echo " 	            <div class='button valuation module'>";
							echo " 	                <a href='/beau-august/valuations/' title='Valuation' class='btn'>";
							echo " 	                    <i class='icon'></i>";
							echo " 	                    <span>Valuation</span>";
							echo " 	                </a>";
							echo " 	            </div>";
							echo " 	            <div class='button offers module'>";
							echo " 	                <a href='/beau-august/offers/' title='Offers' class='btn'>";
							echo " 	                    <i class='icon'></i>";
							echo " 	                    <span>Offers</span>";
							echo " 	                </a>";
							echo " 	            </div>";
							echo " 	            <div class='button service module'>";
							echo " 	                <a href='/beau-august/service-parts-repair/' title='Servicing' class='btn'>";
							echo " 	                    <i class='icon'></i>";
							echo " 	                    <span>Servicing</span>";
							echo " 	                </a>";
							echo " 	            </div>";
							echo " 	            <div class='social-icons module'>";
							echo " 	                <div class='inner' data-social-type='link'>";
							echo " 	                    <div class='title'>";
							echo " 	                        <span>Social</span>";
							echo " 	                    </div>";
							echo " 	                    <ul class=''>";
							echo " 	                        <li class='facebook'>";
							echo " 	                            <a target='_blank' href='' rel='nofollow' title='Facebook' data-click-fingerprint='1.1.1.11.133'>";
							echo " 	                                <i class='icon icon-social icon-facebook'></i>";
							echo " 	                                Facebook";
							echo " 	                            </a>";
							echo " 	                        </li>";
							echo " 	                        <li class='twitter'>";
							echo " 	                            <a target='_blank' href='' rel='nofollow' title='Twitter' data-click-fingerprint='1.1.1.11.133'>";
							echo " 	                                <i class='icon icon-social icon-twitter'></i>";
							echo " 	                                Twitter";
							echo " 	                            </a>";
							echo " 	                        </li>";
							echo " 	                        <li class='googleplus'>";
							echo " 	                            <a target='_blank' href='' rel='nofollow' title='GooglePlus' data-click-fingerprint='1.1.1.11.133'>";
							echo " 	                                <i class='icon icon-social icon-googleplus'></i>";
							echo " 	                                Google+";
							echo " 	                            </a>";
							echo " 	                        </li>";
							echo " 	                    </ul>";
							echo " 	                </div>";
							echo " 	            </div>";
							echo " 	        </div>";
							echo " 	        <pre>" . $styles . "</pre>";
							echo " 	    </div>";
							echo " 	</div>";
							echo "</div>";

						}
					}

					$conn->close();
				?>
			</div>
		</div>

		<div class="container" id="pagination">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>

			<div class="row">
				<h1>Pagination</h1>
				<div class="sub-text">Mouse over the pagination items to see their hover state. To see the transition on the active icons, click <a href="#" class="pag-toggle">Toggle Active Pagination</a></div>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, class, styles FROM pagination";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$class = $row['class'];
							$styles = $row['styles'];

							echo "<div class='span4 cycle-pager-container " . $class . "'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo "	<ul>";
							echo "		<li class='cycle-pager-active'><a href='#'><span>1</span></a></li>";
							echo "		<li class=''><a href='#'><span>2</span></a></li>";
							echo "		<li class=''><a href='#''><span>3</span></a></li>";
							echo "		<li class=''><a href='#'><span>4</span></a></li>";
							echo "	</ul>";
							echo "	<pre>" . $styles . "</pre>";
							echo "</div>";
						}
					}

					$conn->close();
				?>
			</div>

		</div>

		<div class="container" id="social">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>

			<div class="row">
				<h1>Social Icons</h1>
				<div class="sub-text">By default, social icons have their brand-colour set as a background. If you need to set it as a hover instead, you can use a loop. <a href="#" class="html-copy">Click here</a><pre><?php include 'includes/social-hovers.php';?></pre> to copy the LESS. Just throw the variables at the top of your file.</div>
				<pre></pre>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, title, class, styles FROM socialIcons";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$title = $row['title'];
							$class = $row['class'];
							$styles = $row['styles'];

							echo "<div class='span4 social-icons " . $class . " module'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo "	<div class='inner'>";
							echo "		<div class='title'><span>" . $title . "</span></div>";
							echo "		<ul>";
							echo "			<li class='facebook'>";
							echo "				<a target='_blank' href='#' title='Facebook'>";
							echo "					<i class='icon'></i>";
							echo "				</a>";
							echo "			</li>";
							echo "			<li class='twitter'>";
							echo "				<a target='_blank' href='#' title='Twitter'>";
							echo "					<i class='icon'></i>";
							echo "				</a>";
							echo "			</li>";
							echo "			<li class='googleplus'>";
							echo "				<a target='_blank' href='#' title='GooglePlus'>";
							echo "					<i class='icon'></i>";
							echo "				</a>";
							echo "			</li>";
							echo "			<li class='youtube'>";
							echo "				<a target='_blank' href='#' title='YouTube'>";
							echo "					<i class='icon'></i>";
							echo "				</a>";
							echo "			</li>";
							echo "		</ul>";
							echo "		<pre>" . $styles . "</pre>";
							echo "	</div>";
							echo "</div>";
						}
					}

					$conn->close();
				?>
			</div>

		</div>


		<div class="container" id="shapes">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>
			<div class="row">
				<h1>Responsive shapes</h1>
				<div class="sub-text">Click an SVG shape to copy it's markup. You'll need to place it in a custom-html module with the class <span>.shapes</span>. Don't worry, you can set the colour, height and position yourself using <a href='#' class="html-copy">This mixin</a><pre><?php include 'includes/responsive-mixin.php';?></pre>. You can't use transform properties on these SVGs, it won't work in IE.</div>
			</div>
			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, html FROM shapes";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$shape = $row['html'];

							echo " <div class='span4'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo $shape;
							echo "<pre>" . htmlspecialchars($shape) . "</pre>";
							echo "</div>";
						}
					}

					$conn->close();
				?>
			</div>
		</div>

		<div class="container" id="franchises">
			<div class="custom-html shapes">
				<svg version="1.1" viewBox="0 0 100 50" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<path d="M50,49.5C22.386,49.5,0,27.339,0,0v50h100V0C100,27.339,77.613,49.5,50,49.5z"/>
				</svg>
			</div>
			<div class="row">
				<h1>Franchise Badges</h1>
				<div class="sub-text">The base template for franchise-badges <a href="#" class="html-copy">can be found here</a><pre><?php include 'includes/franchise-badges.php';?></pre>, which needs to be imported in your <span>group.less</span>. You'll also need to copy <a href="#" class="html-copy">this mixin</a><pre><?php include 'includes/sprite-mixin.php';?></pre>. You can grab specific hover effects by clicking the examples below. Make sure to copy these styles into <span>.hook-responsive-md</span>, so that the hover effects aren't applied for devices.</div>
			</div>

			<div class="row">
				<?php
					$servername = 'localhost';
					$username = 'root';
					$password = '';
					$dbname = 'Library';

					$conn = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_error) {
						die("Connection failed: " . $conn->connect_error);
					} 

					$sql = "SELECT ID, class, styles FROM franchiseBadges";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {

						while($row = $result->fetch_assoc()) {
							$ID = $row['ID'];
							$class = $row['class'];
							$styles = $row['styles'];

							echo "<div class='span3 jump " . $class .  " module'>";
							echo "<span class='id'>" . $ID . "</span>";
							echo "		<div class='inner'>";
							echo "			<a href='#' title=''>";
							echo " 				<span class='content'>";
							echo "					<span></span>";
							echo "				</span>";
							echo " 				<span class='title'>";
							echo "					<span>Franchise Name</span>";
							echo "				</span>";
							echo "			</a>";
							echo "			<pre>" . $styles . "</pre>";
							echo "		</div>";
							echo "    </div>";
						}
					}

					$conn->close();
				?>
			</div>

		</div>

		<script src="js/scripts.js"></script>

	</body>
</html>