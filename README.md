# README #


### What is this repository for? ###

* This repository is the storage place for all the LESS files necessary to style the CSS Resource Library

### How do I get set up? ###

* Simply git clone this repository, and set yourself up in your text editor. 


### Contribution guidelines ###

* Test your LESS before you commit!
* Make it as lean as possible. 
* Make sure you write your LESS in the correct partials
* Once pushed, contact Beau August to have new element added to database

### Who do I talk to? ###

* Beau August